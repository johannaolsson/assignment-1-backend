package items;

import characters.EquipmentSlots;
import characters.PrimaryAttribute;
import items.armor.ArmorTypes;
import items.weapons.WeaponTypes;

public abstract class Item {

    // Variables for the Item class.
    private String name;
    private int requiredLevel;
    private EquipmentSlots slot;
    private WeaponTypes weaponType;
    private ArmorTypes armorType;
    private PrimaryAttribute attributes = new PrimaryAttribute();

    // Gets attributes and return it.
    public PrimaryAttribute getAttributes() {
        return attributes;
    }

    // Gets name and return it.
    public String getName() {
        return name;
    }

    // Getter and setter for EquipmentSlots.
    public EquipmentSlots getSlot() {
        return slot;
    }
    public void setSlot(EquipmentSlots slot) {
        this.slot = slot;
    }

    // Getter and setter for requiredLevel.
    public int getRequiredLevel() {
        return requiredLevel;
    }
    public void setRequiredLevel(int requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    // Getter and setter for weaponType.
    public void setWeaponType(WeaponTypes weaponType) {
        this.weaponType = weaponType;
    }
    public WeaponTypes getWeaponType() {
        return weaponType;
    }

    // Getter and setter for armorType.
    public void setArmorType(ArmorTypes armorType) {
        this.armorType = armorType;
    }
    public ArmorTypes getArmorType() {
        return armorType;
    }

    // Sets name of the item.
    public Item(String itemName) {
        this.name = itemName;
    }

    // Sets attributes of the item.
    public Item(String itemName, int strength, int dexterity, int intelligence) {
        this.name = itemName;
        attributes.setStrength(strength);
        attributes.setDexterity(dexterity);
        attributes.setIntelligence(intelligence);

    }
}
