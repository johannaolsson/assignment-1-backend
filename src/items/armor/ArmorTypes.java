package items.armor;

// Creates enum for ArmorTypes.
public enum ArmorTypes {
    Cloth,
    Leather,
    Mail,
    Plate,
}