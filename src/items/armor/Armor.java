package items.armor;
import characters.PrimaryAttribute;
import items.Item;

public class Armor extends Item {

    public Armor(String armorName, int requiredLevel, ArmorTypes armorType) {

        super(armorName);
        // Sets the required level and the armor type.
        this.setRequiredLevel(requiredLevel);
        this.setArmorType(armorType);
    }
}
