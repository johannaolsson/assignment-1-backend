package items.armor;

// Prints error messages for InvalidArmorException.
public class InvalidArmorException extends Exception{
    public InvalidArmorException(String message) {
        super(message);
        System.out.println(message);
    }
}