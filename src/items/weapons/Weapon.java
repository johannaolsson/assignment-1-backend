package items.weapons;
import items.Item;

public class Weapon extends Item{
    private float dps;
    public Weapon(String weaponName, int damage, float attackSpeed, int requiredLevel, WeaponTypes weaponType) {

        super(weaponName);

        this.dps = damage * attackSpeed; // Calculates the dps of the weapon.
        // Sets the required level and the weapon type.
        this.setRequiredLevel(requiredLevel);
        this.setWeaponType(weaponType);

    }

    // Gets the dps of the weapon.
    public float getDPS() {
        return dps;
    }
}
