package items.weapons;

// Prints error messages for InvalidWeaponException.
public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String message) {
        super(message);
        System.out.println(message);
    }
}
