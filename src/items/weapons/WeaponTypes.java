package items.weapons;

// Creates enum for WeaponTypes.
public enum WeaponTypes {
    Axes,
    Bows,
    Daggers,
    Hammers,
    Staffs,
    Swords,
    Wands,
}