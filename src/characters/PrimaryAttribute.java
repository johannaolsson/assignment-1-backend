package characters;

public class PrimaryAttribute {
    // Variables for the PrimaryAttribute class.
    private int strength, dexterity, intelligence;
    public PrimaryAttribute(){
    }

    // Adds the attributes.
    public void addToAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    // Sets the attributes.
   public void setToAttributes(int strength, int dexterity, int intelligence) {
       this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // Merges the attributes.
    public void mergeAttributes(PrimaryAttribute attributes1, PrimaryAttribute attributes2) {
       this.strength = attributes1.getStrength() + attributes2.getStrength();
       this.dexterity = attributes1.getDexterity() + attributes2.getDexterity();
       this.intelligence = attributes1.getIntelligence() + attributes2.getDexterity();
   }

   // Getter and setter for attributes.
    public int getStrength() {
        return strength;
    }
    public void setStrength(int strength) {
        this.strength = strength;
    }
    public int getDexterity() {
        return dexterity;
    }
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
    public int getIntelligence() {
        return intelligence;
    }
    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
