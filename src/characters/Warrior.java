package characters;

import items.armor.ArmorTypes;
import items.weapons.WeaponTypes;

public class Warrior extends Character {
    // Variables for the Warrior class.
    private WeaponTypes[] weaponList = {WeaponTypes.Axes, WeaponTypes.Hammers, WeaponTypes.Swords};
    private ArmorTypes[] armorList = {ArmorTypes.Mail, ArmorTypes.Plate};

    // Sets the list of weapons and armor and Warrior attributes.
    public Warrior (String name) {
        super(name, 5,2, 1);
        setArmorList(armorList);
        setWeaponList(weaponList);
    }

    // Increases Warriors attributes if leveling up.
    public void levelUp() {
        levelUp(3, 2, 1);
    }

    // Calculates the DPS of the character.
    @Override
    public float calculateDPS(float weaponDPS) {
        return weaponDPS * (1 + totalAttribute.getStrength()/100);
    }
}
