package characters;

import items.armor.ArmorTypes;
import items.weapons.WeaponTypes;

public class Rogue extends Character {
    // Variables for the Rogue class.
    private WeaponTypes[] weaponList = {WeaponTypes.Daggers, WeaponTypes.Swords};
    private ArmorTypes[] armorList = {ArmorTypes.Leather, ArmorTypes.Mail};

    // Sets the list of weapons and armor and Rogue attributes.
    public Rogue (String name) {
        super(name, 2,6, 1);
        setArmorList(armorList);
        setWeaponList(weaponList);
    }

    // Increases Rogues attributes if leveling up.
    public void levelUp() {
        levelUp(1, 4, 1);
    }

    // Calculates the DPS of the character.
    @Override
    public float calculateDPS(float weaponDPS) {
        return weaponDPS * (1 + totalAttribute.getDexterity()/100);
    }
}
