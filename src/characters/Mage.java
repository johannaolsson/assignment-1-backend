package characters;

import items.armor.ArmorTypes;
import items.weapons.WeaponTypes;

public class Mage extends Character {
    // Variables for the Mage class.
    private WeaponTypes[] weaponList = {WeaponTypes.Staffs, WeaponTypes.Wands};
    private ArmorTypes[] armorList = {ArmorTypes.Cloth};

    // Sets the list of weapons and armor and Mage attributes.
    public Mage(String name) {
        super(name, 1, 1, 8);
        setArmorList(armorList);
        setWeaponList(weaponList);
    }

    // Increases Mages attributes if leveling up.
    public void levelUp() {
        levelUp(1, 1, 5);
    }

    // Calculates the DPS of the character.
    @Override
    public float calculateDPS(float weaponDPS) {
        return weaponDPS * (1 + (float) totalAttribute.getIntelligence() / 100);
    }
}
