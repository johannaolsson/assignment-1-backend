package characters;

import items.armor.ArmorTypes;
import items.weapons.WeaponTypes;

public class Ranger extends Character {
    // Variables for the Ranger class.
    private WeaponTypes[] weaponList = {WeaponTypes.Bows};
    private ArmorTypes[] armorList = {ArmorTypes.Leather, ArmorTypes.Mail};

    // Sets the list of weapons and armor and Ranger attributes.
    public Ranger (String name) {
        super(name, 1,7, 1);
        setArmorList(armorList);
        setWeaponList(weaponList);
    }

    // Increases Rangers attributes if leveling up.
    public void levelUp() {
        levelUp(1, 5, 1);
    }

    // Calculates the DPS of the character.
    @Override
    public float calculateDPS(float weaponDPS) {
        return weaponDPS * (1 + totalAttribute.getDexterity()/100);
    }
}
