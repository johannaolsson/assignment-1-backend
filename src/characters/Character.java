package characters;

import items.Item;
import items.armor.Armor;
import items.armor.ArmorTypes;
import items.armor.InvalidArmorException;
import items.weapons.InvalidWeaponException;
import items.weapons.Weapon;
import items.weapons.WeaponTypes;

import java.util.HashMap;

public abstract class Character {
    // Variables for the Character class.
    private String name;
    private int level = 1;
    private ArmorTypes[] armorList = {};
    private WeaponTypes[] weaponList = {};


    // Getter and setter methods for armor and weapon.
    public ArmorTypes[] getArmorList() {
        return armorList;
    }

    public void setArmorList(ArmorTypes[] armorList) {
        this.armorList = armorList;
    }

    public WeaponTypes[] getWeaponsList() {
        return weaponList;
    }

    public void setWeaponList(WeaponTypes[] weaponsList) {
        this.weaponList = weaponsList;
    }

    // Return the name of the character.
    public String getName() {
        return name;
    }

    // Creates new PrimaryAttributes.
    PrimaryAttribute baseAttribute = new PrimaryAttribute();
    PrimaryAttribute itemAttribute = new PrimaryAttribute();
    PrimaryAttribute totalAttribute = new PrimaryAttribute();

    // Add the strength, dexterity and intelligence attributes.
    public Character(String name, int strength, int dexterity, int intelligence) {
        this.name = name;
        baseAttribute.addToAttributes(strength, dexterity, intelligence);
    }

    // Method that increase attributes and level.
    public void levelUp(int strengthIncrease, int dexterityIncrease, int intelligenceIncrease) {
        baseAttribute.addToAttributes(strengthIncrease, dexterityIncrease, intelligenceIncrease);
        level += 1;
    }

    // Getter for level.
    public int getLevel() {
        calculateTotal();
        return level;
    }

    // Merge itemAttribute with baseAttribute to totalAttribute.
    public void totalAttributes() {
        itemAttribute.setToAttributes(0, 0, 0);

        slots.forEach((slots, item) -> itemAttribute.addToAttributes(item.getAttributes().getStrength(), item.getAttributes().getDexterity(), item.getAttributes().getIntelligence()));
        totalAttribute.mergeAttributes(itemAttribute, baseAttribute);
    }

    // Sets baseAttribute.
    public void calculateTotal() {
        totalAttribute = baseAttribute;
        baseAttribute.addToAttributes(itemAttribute.getStrength(), itemAttribute.getDexterity(), itemAttribute.getIntelligence());
    }


    // Creates HashMap for the slots.
    HashMap<EquipmentSlots, Item> slots = new HashMap<>();

    // Return slots.
    public HashMap<EquipmentSlots, Item> getSlots() {
        return slots;
    }

    // Looping through armorList and weaponList and checking if character is allowed to equip item.
    public void Equipments(EquipmentSlots slot, Item item) throws InvalidArmorException, InvalidWeaponException {

        if (item.getClass().equals(Armor.class)) {
            for (int i = 0; i < getArmorList().length; i++) {

                if (getArmorList()[i].equals(item.getArmorType()) && level >= item.getRequiredLevel()) {
                    slots.put(slot, item);
                    totalAttributes();
                    break;
                }
            }
            if (slots.get(slot) != item) {
                throw new InvalidArmorException("The armor can not be equipped"); // Error message.
            }
        }

        if (item.getClass().equals(Weapon.class)) {
            for (int i = 0; i < getWeaponsList().length; i++) {
                if (getWeaponsList()[i].equals(item.getWeaponType()) && level >= item.getRequiredLevel()) {
                    slots.put(slot, item);
                    totalAttributes();
                    break;
                }
            }
            if (slots.get(slot) != item) {
                throw new InvalidWeaponException("The weapon can not be equipped"); // Error message.
            }
        }
    }

    // Creates StringBuilder for display the list.
    StringBuilder displayAll = new StringBuilder();

    // Displays specific information about a character.
    public String display() {

        displayAll.append("Name " + name + " ");
        displayAll.append("Level " + level + " ");
        displayAll.append("Strength " + baseAttribute.getStrength() + " ");
        displayAll.append("Dexterity " + baseAttribute.getDexterity() + " ");
        displayAll.append("Intelligence " + baseAttribute.getIntelligence() + " ");
        displayAll.append(getCharacterDPS());

        return displayAll.toString();
    }


    // Calculates weaponDPS.
    protected abstract float calculateDPS(float weaponDPS);

    // Return 1 if weapon is null, otherwise it returns weaponDPS.
    public float getWeaponDPS() {
        Weapon weapon = (Weapon) slots.get(EquipmentSlots.Weapon);
        if (weapon == null) return calculateDPS(1);
        return weapon.getDPS();
    }

    // Get WeaponDPS.
    public float getCharacterDPS() {
        return calculateDPS(getWeaponDPS());
    }

}