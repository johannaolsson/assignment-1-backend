package characters;

// Creates enum for EquipmentSlots.
public enum EquipmentSlots {
    Head,
    Body,
    Legs,
    Weapon,
}
