package characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    @Test
    void TestStartLevel_Mage_ShouldBeLevelOne() {
        Mage character = new Mage("Johanna");
        character.getLevel();

        assertEquals(1, character.getLevel());
    }


    @Test
    void TestGainLevel_Mage_ShouldBeLevelTwo() {
        Mage character = new Mage("Johanna");
        character.levelUp();

        assertEquals(2, character.getLevel());
    }


    @Test
    void TestDefaultAttributes_Mage_ShouldBeDefault() {
        Mage characterMage = new Mage("Johanna");

        assertEquals(1, characterMage.baseAttribute.getStrength());
        assertEquals(1, characterMage.baseAttribute.getDexterity());
        assertEquals(8, characterMage.baseAttribute.getIntelligence());

    }


    @Test
    void TestDefaultAttributes_Ranger_ShouldBeDefault() {
        Ranger characterRanger = new Ranger("Johanna");

        assertEquals(1, characterRanger.baseAttribute.getStrength());
        assertEquals(7, characterRanger.baseAttribute.getDexterity());
        assertEquals(1, characterRanger.baseAttribute.getIntelligence());

    }

    @Test
    void TestDefaultAttributes_Rogue_ShouldBeDefault() {
        Rogue characterRogue = new Rogue("Johanna");

        assertEquals(2, characterRogue.baseAttribute.getStrength());
        assertEquals(6, characterRogue.baseAttribute.getDexterity());
        assertEquals(1, characterRogue.baseAttribute.getIntelligence());

    }

    @Test
    void TestDefaultAttributes_Warrior_ShouldBeDefault() {
        Warrior characterWarrior = new Warrior("Johanna");

        assertEquals(5, characterWarrior.baseAttribute.getStrength());
        assertEquals(2, characterWarrior.baseAttribute.getDexterity());
        assertEquals(1, characterWarrior.baseAttribute.getIntelligence());

    }





    @Test
    void TestIncreasedAttributes_Mage_ShouldBeIncreased() {
        Mage characterMage = new Mage("Johanna");
        characterMage.levelUp();


        assertEquals(2, characterMage.getLevel());
        assertEquals(2, characterMage.totalAttribute.getStrength());
        assertEquals(2, characterMage.totalAttribute.getDexterity());
        assertEquals(13, characterMage.totalAttribute.getIntelligence());

    }

    @Test
    void TestIncreasedAttributes_Ranger_ShouldBeIncreased() {
        Ranger characterRanger = new Ranger("Johanna");
        characterRanger.levelUp();


        assertEquals(2, characterRanger.getLevel());
        assertEquals(2, characterRanger.totalAttribute.getStrength());
        assertEquals(12, characterRanger.totalAttribute.getDexterity());
        assertEquals(2, characterRanger.totalAttribute.getIntelligence());

    }

    @Test
    void TestIncreasedAttributes_Rogue_ShouldBeIncreased() {
        Rogue characterRogue = new Rogue("Johanna");
        characterRogue.levelUp();


        assertEquals(2, characterRogue.getLevel());
        assertEquals(3, characterRogue.totalAttribute.getStrength());
        assertEquals(10, characterRogue.totalAttribute.getDexterity());
        assertEquals(2, characterRogue.totalAttribute.getIntelligence());

    }

    @Test
    void TestIncreasedAttributes_Warrior_ShouldBeIncreased() {
        Warrior characterWarrior = new Warrior("Johanna");
        characterWarrior.levelUp();


        assertEquals(2, characterWarrior.getLevel());
        assertEquals(8, characterWarrior.totalAttribute.getStrength());
        assertEquals(4, characterWarrior.totalAttribute.getDexterity());
        assertEquals(2, characterWarrior.totalAttribute.getIntelligence());

    }



}