package items;

import characters.EquipmentSlots;
import characters.Warrior;
import items.armor.Armor;
import items.armor.ArmorTypes;
import items.armor.InvalidArmorException;
import items.weapons.InvalidWeaponException;
import items.weapons.Weapon;
import items.weapons.WeaponTypes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    @Test
    void TestHighLevel_Warrior_WeaponExceptionShouldBeThrown() {
        Warrior characterWarrior = new Warrior("Johanna");
        Weapon testWeapon = new Weapon("weapon1", 1, 1, 2, WeaponTypes.Axes);

        assertThrows(InvalidWeaponException.class, () -> characterWarrior.Equipments(EquipmentSlots.Weapon, testWeapon));
    }

    @Test
    void TestHighLevel_Warrior_ArmorExceptionShouldBeThrown() {
        Warrior characterWarrior = new Warrior("Johanna");
        Armor testArmor = new Armor("armor1", 2, ArmorTypes.Plate);

        assertThrows(InvalidArmorException.class, () -> characterWarrior.Equipments(EquipmentSlots.Body, testArmor));
    }

    @Test
    void TestWrongType_Warrior_WeaponExceptionShouldBeThrown() {
        Warrior characterWarrior = new Warrior("Johanna");
        Weapon testWeapon = new Weapon("weapon1", 1, 1, 1, WeaponTypes.Bows);

        assertThrows(InvalidWeaponException.class, () -> characterWarrior.Equipments(EquipmentSlots.Weapon, testWeapon));
    }

    @Test
    void TestWrongType_Warrior_ArmorExceptionShouldBeThrown() {
        Warrior characterWarrior = new Warrior("Johanna");
        Armor testArmor = new Armor("armor1", 2, ArmorTypes.Cloth);

        assertThrows(InvalidArmorException.class, () -> characterWarrior.Equipments(EquipmentSlots.Body, testArmor));
    }

    @Test
    void TestValidWeapon_Warrior_ShouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior characterWarrior = new Warrior("Johanna");
        Weapon testWeapon = new Weapon("weapon1", 1, 1, 1, WeaponTypes.Axes);
        characterWarrior.Equipments(EquipmentSlots.Weapon, testWeapon);

        assertEquals(testWeapon, characterWarrior.getSlots().get(EquipmentSlots.Weapon));
    }

    @Test
    void TestValidArmor_Warrior_ShouldReturnTrue() throws InvalidArmorException, InvalidWeaponException {
        Warrior characterWarrior = new Warrior("Johanna");
        Armor testArmor = new Armor("armor1", 1, ArmorTypes.Mail);
        characterWarrior.Equipments(EquipmentSlots.Body, testArmor);

        assertEquals(testArmor, characterWarrior.getSlots().get(EquipmentSlots.Body));
    }

    @Test
    void TestNoWeapon_Warrior_CalculateDPS() {
        Warrior characterWarrior = new Warrior("Johanna");
        float expectedDPS = characterWarrior.getWeaponDPS();

        assertEquals(expectedDPS, 1 * (1 + (5/100)));

    }

    @Test
    void TestValidWeapon_Warrior_CalculateDPS() throws InvalidArmorException, InvalidWeaponException {
        Warrior characterWarrior = new Warrior("Johanna");
        Weapon testWeapon = new Weapon("weapon1", 7, 1, 1, WeaponTypes.Axes);
        characterWarrior.Equipments(EquipmentSlots.Weapon, testWeapon);
        float expectedDPS = characterWarrior.getWeaponDPS();

        assertEquals(expectedDPS, 7 * (1 + (5/100)));
    }

    @Test
    void TestValidWeaponAndArmor_Warrior_CalculateDPS() throws InvalidArmorException, InvalidWeaponException {
        Warrior characterWarrior = new Warrior("Johanna");
        Weapon testWeapon = new Weapon("weapon1", 7, 1.1f, 1, WeaponTypes.Axes);
        Armor testArmor = new Armor("armor1",1, ArmorTypes.Mail);
        characterWarrior.Equipments(EquipmentSlots.Weapon, testWeapon);
        characterWarrior.Equipments(EquipmentSlots.Body, testArmor);
        float expectedDPS = characterWarrior.calculateDPS(characterWarrior.getWeaponDPS());

        assertEquals((7 * 1.1f) * (1 + ((5 + 1)/100)), expectedDPS);
    }

}