# Java Fundamental Assignment - RPG Characters

This project is an application that represent a Role Play Game. In the game there is four different characters which is Mage, Ranger, Rogue and Warrior. The characters have different types of attributes; strength, dexterity and intelligence.  A character could also equip items like armors and weapons.

## Install

- Install JDK 17
- Install Intellij
- Clone repository:

```
https://gitlab.com/johannaolsson/assignment-1-backend.git
```


## Usage

In Main.java you can create a new character and add a weapon or armor. You can also interact with the application by for example using ```display()``` or ```levelUp()``` . Run CharacterTest and the ItemTest to test the application.

## Contributor

Johanna Olsson [@johannaolsson](https://gitlab.com/johannaolsson)


